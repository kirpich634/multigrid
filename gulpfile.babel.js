import gulp from 'gulp'
import autoprefixer from "gulp-autoprefixer"
import sass from 'gulp-sass'
import browserSync from "browser-sync"
import sourcemaps from "gulp-sourcemaps"
import path from "path"
import debug from "gulp-debug"

const createdBrowserSync = browserSync.create();

const tasks = {
    multigridBuild: "multigrid-build",
    demoSCSSBuild: "demo-scss-build",
    watch: "watch",
    browserSync: "browser-sync",
    dev: "dev"
};

gulp.task(tasks.multigridBuild, () => gulp.src(path.resolve('./src/**/*.scss'))
    .pipe(debug({title: 'multigrid-build'}))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest("./dist"))
);

gulp.task(tasks.demoSCSSBuild, () => gulp.src("./demo/src/scss/**/*.scss")
    .pipe(debug({title: 'sass-compile'}))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./demo/dist/css"))
);

gulp.task(tasks.watch, () => {
    gulp.watch("./src/**/*.scss", gulp.series("multigrid-build"));
    gulp.watch(["./src/**/*.scss", "./demo/src/scss/**/*.scss", ], gulp.series("demo-scss-build"));
});

gulp.task(tasks.browserSync, () => {
    createdBrowserSync.init({
        server: {
            baseDir: "./demo"
        }
    });

    createdBrowserSync.watch(["./demo/dist/**/*.*", "./demo/**/*.html"]).on('change', createdBrowserSync.reload)
});

gulp.task(tasks.dev, gulp.series(
    gulp.parallel(
        tasks.multigridBuild,
        tasks.demoSCSSBuild
    ),
    gulp.parallel(
        tasks.watch,
        tasks.browserSync
    )
));
